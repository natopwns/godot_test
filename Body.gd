extends KinematicBody

const GRAVITY = 12.0

var velocity = Vector3()
var on_floor = false

onready var OnFloorArea = $OnFloorArea

func check_floor_change():
	
	var on_floor_this_frame = OnFloorArea.get_overlapping_bodies().size() > 1
	
	if on_floor_this_frame != on_floor:
		print(name + " On floor: " + str(on_floor_this_frame))
	
	on_floor = on_floor_this_frame

func _physics_process(delta):
	
	check_floor_change()
	
	velocity.y -= GRAVITY * delta
	velocity = move_and_slide(velocity, Vector3.UP)
